import styled from "styled-components";
import { Splide, SplideSlide } from "@splidejs/react-splide";

import "@splidejs/react-splide/css";

const Wrapper = styled.div`
  margin: 4rem 0;
`;

const Card = styled.div`
  /* max-height: 25rem; */
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 1.5rem;
  /* width: 100%; */
  overflow: hidden;
  img {
    max-height: inherit;
    /* height: 100%; */
    /* width: 100%; */
    object-fit: contain;
    border-radius: inherit;
  }
  p {
    position: absolute;
    z-index: 10;
    left: 50%;
    bottom: 0;
    transform: translate(-50%, 0);
    font-size: 1.5rem;
    text-align: fill;
    font-weight: 700;
    /* mix-blend-mode: difference; */
    color: #fff;
    padding: 1rem;
    text-align: center;
  }
`;

const Gradient = styled.div`
  position: absolute;
  min-height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.2));
  border-radius: inherit;
`;

const Popular = ({ beerNames }) => {
  return (
    <>
      <Wrapper>
        <h3>Popular ones</h3>
        <Splide
          options={{
            perPage: 3,
            drag: "free",
            arrows: false,
            gap: "1rem",
            breakpoints: {
              1024: {
                perPage: 3,
              },
              767: {
                perPage: 2,
              },
              640: {
                perPage: 1,
              },
            },
          }}
        >
          {beerNames.map((beer, index) => (
            <SplideSlide key={index}>
              <Card>
                <p>{beer.strCategory}</p>
                <img src={beer.strCategoryThumb} alt={beer.strCategory}></img>
                <Gradient />
              </Card>
            </SplideSlide>
          ))}
        </Splide>
      </Wrapper>
    </>
  );
};

export default Popular;
