import styled from "styled-components";
import { FaSearch } from "react-icons/fa";
import { useState } from "react";

const StyledForm = styled.form`
  /* margin: 0 20px; */
  position: relative;
  width: 100%;
  input {
    border: none;
    font-size: 1.5rem;
    padding: 1rem 3rem;
    border-radius: 1rem;
    outline: solid;
    width: -webkit-fill-available;
  }
  svg {
    position: absolute;
    left: 1rem;
    top: 50%;
    transform: translate(0, -50%);
  }
`;

function SearchBar() {
  const [searchText, setSearchText] = useState("");

  const submitHandler = (evt) => {
    evt.preventDefaults();
  };

  return (
    <StyledForm autoComplete="off" onSubmit={submitHandler}>
      <FaSearch />
      <input
        type="text"
        name="search"
        value={searchText}
        onChange={(e) => {
          setSearchText(e.target.value);
        }}
      />
    </StyledForm>
  );
}

export default SearchBar;
