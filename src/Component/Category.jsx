import styled from "styled-components";
import { NavLink } from "react-router-dom";

const List = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: 1rem;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 1.5rem 0;
  svg {
    font-size: 2rem;
  }
  h4 {
    font-size: 1.25rem;
  }
`;

function Category({ categories = [] }) {
  return (
    <>
      <h3>Categories</h3>
      <List>
        {categories.map((x) => (
          <NavLink key={x.strArea} to={`cuisine/${x.strArea}`}>
            <h4>{x.strArea}</h4>
          </NavLink>
        ))}
      </List>
    </>
  );
}

export default Category;
