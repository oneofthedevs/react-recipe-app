import { styled } from "styled-components";
import { motion } from "framer-motion";
import { Link, useParams } from "react-router-dom";
import { useCallback, useEffect, useState } from "react";
import { routes } from "../Common/Constants";
import { MdOutlineArrowBackIosNew } from "react-icons/md";

const Card = styled.div`
  max-height: 10rem;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 1.5rem;
  /* width: 100%; */
  overflow: hidden;
  img {
    max-height: inherit;
    /* height: 100%; */
    /* width: 100%; */
    object-fit: contain;
    border-radius: inherit;
  }
  p {
    position: absolute;
    z-index: 10;
    left: 50%;
    bottom: 0;
    transform: translate(-50%, 0);
    font-size: 1.5rem;
    text-align: fill;
    font-weight: 700;
    /* mix-blend-mode: difference; */
    color: #fff;
    padding: 1rem;
    text-align: center;
  }
`;

const Gradient = styled.div`
  position: absolute;
  min-height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.2));
  border-radius: inherit;
`;

const Container = styled.main`
  /* margin: 0 20%; */
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
`;

const Heading = styled.h1`
  font-size: 1.5rem;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  p {
    margin: 0 0 0 1rem;
  }
`;

function Cuisine() {
  let params = useParams();
  const [dishes, setDishes] = useState([]);

  const getDishes = useCallback(async () => {
    if (params.id) {
      const response = await fetch(
        `${routes.baseURL}filter.php?a=${params.id}`
      );
      const data = await response.json();
      setDishes(data.meals);
    }
  }, [params.id]);

  useEffect(() => {
    getDishes();
  }, [getDishes]);

  return (
    <>
      <Heading>
        <Link to={"/"}>
          <MdOutlineArrowBackIosNew />
        </Link>
        <p>{params.id}</p>
      </Heading>
      <Container>
        {dishes.map((meal) => (
          <Link to={`/dish/${meal.idMeal}`} key={meal.idMeal}>
            <Card>
              <p>{meal.strMeal}</p>
              <img
                src={meal.strMealThumb}
                alt={meal.strMeal}
                loading="lazy"
              ></img>
              <Gradient />
            </Card>
          </Link>
        ))}
      </Container>
    </>
  );
}

export default Cuisine;
