import Home from "./Home";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import "../App.css";
import Cuisine from "./Cuisine";
import SearchedMeals from "./SearchedMeals";

function Pages() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Cuisine/:id" element={<Cuisine />} />
        <Route path="/Dish/:id" element={<SearchedMeals />} />
        <Route path="/Search/:text" element={<SearchedMeals />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Pages;
