/* eslint-disable no-undef */
import { useEffect, useState, useCallback } from "react";
import { routes } from "../Common/Constants";
import Popular from "../Component/Popular";
import Category from "../Component/Category";
import SearchBar from "../Component/SearchBar";

function Home() {
  const [popular, setPopular] = useState([]);
  const [categories, setCategory] = useState([]);

  const getPopularRecipes = useCallback(async () => {
    try {
      if (localStorage.getItem("popular")) {
        const data = localStorage.getItem("popular");
        setPopular(JSON.parse(data));
      } else {
        const response = await fetch(`${routes.baseURL}categories.php`);
        const data = await response.json();
        console.log(data.categories);
        setPopular(data.categories);
      }
    } catch (ex) {
      console.error(ex);
    }
  }, []);

  const getCategories = useCallback(async () => {
    try {
      if (localStorage.getItem("areas")) {
        const data = localStorage.getItem("areas");
        setCategory(JSON.parse(data));
      } else {
        const response = await fetch(`${routes.baseURL}list.php?a=list`);
        const data = await response.json();

        console.log(data.meals);
        setCategory(data.meals);
      }
    } catch (ex) {
      console.error(`Error -> ${ex}`);
    }
  }, []);

  /* UseEffect runs based on second param
   * [] = Only run when component is mounted/Rendered
   * [<variable>] = Runs every time variable changes
   */

  useEffect(() => {
    getPopularRecipes();
    getCategories();
    console.log("Home useEffect called");
  }, [getCategories, getPopularRecipes]);

  useEffect(() => {
    localStorage.setItem("popular", JSON.stringify(popular));
  }, [popular]);

  useEffect(() => {
    localStorage.setItem("areas", JSON.stringify(categories));
  }, [categories]);

  return (
    <>
      <Popular beerNames={popular} />
      <SearchBar />
      <Category categories={categories} />
    </>
  );
}

export default Home;
